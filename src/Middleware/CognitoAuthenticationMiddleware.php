<?php
namespace pmill\LaravelAwsCognito\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use pmill\LaravelAwsCognito\ApiGuard;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class CognitoAuthenticationMiddleware {
  const Guard_Name = 'aws-cognito';
  /**
   * @param Request $request
   * @param Closure $next
   *
   * @return Response
   * @throws AuthorizationException
   */
  public function handle($request, Closure $next) {
    config('auth.defaults.guard', self::Guard_Name);

    /** @var ApiGuard $guard */
    $guard = Auth::guard(self::Guard_Name);

    if (!$guard->validateToken()) {
      throw new UnauthorizedHttpException('invalid_token');
    }

    /** @var Response $response */
    return $next($request);
  }
}